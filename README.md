readme file for RTIInterfacecapturingTool

The tool is developed to measure the instability growth of the Rayleigh-Taylor instability. The effect is described in the attached master thesis. 
The sources for the tool are found in the 'src' folder: calling 'make' triggers the compilation as defined in Makefile. The tool can then be executed through ./interfaceTrackerOut.
The main() function is found in interfaceTracker.ccp.
An example data set 'interfaceTrackingTestiLenx200_Rho.csv' was added illustrating the fine fitting qualities for early iteration times and the late iteration times where the tool is no longer applicable.

requirements: openMp, gnuPlot
