// readingData.cpp

#include <cstdlib>			// only for atof()
#include <sstream>

ifstream strDat;
string strBuffer;
int lenAr;		 
double *rho;

bool loadDataStream(const string& strIn){
	strDat.open (strIn.c_str(), ifstream::in);
	if (! strDat.is_open() ){
		cout << "loadData	reading data file failed: file not found. aborting." << endl;
		return false;
	}

	// counting elements in one line to set density array to correct size
	ifstream strDatInterim;
	stringstream stStrIn;
	strDatInterim.open (strIn.c_str(), ifstream::in);
	getline(strDatInterim, strBuffer);
	strDatInterim.close();
	stStrIn.str(strBuffer);
	int iCounter = 0;
	while( getline(stStrIn, strBuffer,',').good()  ){
		iCounter++;
	}
	lenAr = iCounter+1;
	rho = new double[lenAr];
	return true;
}


//returns false if empty line is read
bool loadField(){
	// cout << "loadField\tloading density field" << endl;
	cout.precision(7);
	int iCounter = 0;

	//loading one line from stream: a single time step field	
	if( ! getline(strDat, strBuffer) ) return false;

	stringstream stStrIn;
	stStrIn.str(strBuffer); 		// making the one-line-string strBuffer a stream again such that it can be cut in pieces by getline with a dilimeter ',' (because string::stod has a bug)

	while( getline(stStrIn, strBuffer,',').good() ){
		rho[iCounter] = atof(strBuffer.c_str());
		// cout << iCounter << "\t" << rho[iCounter] << endl;
		iCounter++;
	}
	rho[iCounter] = atof(strBuffer.c_str());		// last entry is not set within while loop
	// cout << iCounter << "\t" << rho[iCounter] << endl;

	return true;
}



