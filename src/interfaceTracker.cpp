#include "postProcessing.h"

using namespace std;

// this included file only contains global definitions and functions that become redundant when this code is merged with the actual simulation code.
#include "readingData.cpp"

int main(){
	// cout << "Hello world, I am interfaceTracker.cpp" << endl;
	
	// loading simulation data. not inclulded in post processing class since simulation data is readily avalible in the memory when this code will be merged with the simulation scheme.
	if(! loadDataStream( "interfaceTrackingTestiLenX200_Rho.csv" ) ) return 1;

	postProcessing pp(rho);

	int counter = 0;
	while( loadField() ){
		if(pp.DEBUG)	cout << "analysing time step " << counter << endl;
		
		pp.fit();	// the cosine function is fitted against the density field
		
		if( counter == 0 || counter == 20 || counter == 40 || counter == 60 || counter == 80 || counter == 100 ){
			cout << "step: " << counter << "\tfitted amplitude: " << pp.dAmplIntf[counter] << "\twith deviance; " << pp.devianceIntf[counter] << endl;
			pp.gnuPlot();
		}
		counter++;
	}
	return 0;
}

		