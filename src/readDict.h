//readDict.h

#include <fstream>
#include <iostream>
#include <string>
using namespace std;

class readDict{

	private:
		string strDictName;
		string strKeyWordValue;
		fstream iStrDict;
		
		void stringToType( const string& strInput, bool& typeValue);			// function overloading
		void stringToType( const string& strInput, int& typeValue);
		void stringToType( const string& strInput, double& typeValue);
		void stringToType( const string& strInput, string& typeValue);
		string findKeyWordValue(const string& strKeyword);

	public:		
		bool bRDFail;

		readDict(const string& strName);
		bool toBool(const string& strTB);
		void findAndReplace(const string& strFind, const string& strReplace);
		void setGPParameter(const string& strFind, const string& strReplace);

		// template function definition must be here. Ugly but compiler won't compile if definition is in cpp file
		template <class T> T getKeyWordValue(const string& strKeyWord){
			strKeyWordValue.clear();
			T tVariable;
			const string strBuffer = findKeyWordValue(strKeyWord);
			stringToType(strBuffer, tVariable);
			if( bRDFail ){
				cout << "readDict::getKeyWordValue\tERROR: importing of " << strKeyWord << " failed." << endl;
				bRDFail = false;
			}else{
				cout << "readDict::getKeyWordValue\tkey word: " << strKeyWord << ", imported variable: " << tVariable << endl; 
			}
			return tVariable;
		}
};