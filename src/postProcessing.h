//postProcessing.h

#include "readDict.h"
#include "math.h"
#include "omp.h"
#include <iostream>
#include <vector>
#include <cstdio>		// only for printf in parallel sections
using namespace std;
#define MEGA 1000000

class postProcessing{

	private:
		int iLenX;
		int iLenY;
		int iLenAr;
		int iIntfWidth;
		int iLambda;
		int iThreadNo;
		int noFittingIterations;	

		double dRelPertAmpl;
		double dAtwoodNo;
		double dS;
		double dKPrime;
	
		bool bPrintIntfProp;
		bool bPrintDropletProp;
		bool bPrintBubbleProp;
		bool bGnuPlot;
		bool bParallel;

		double* dField;
		double midY;
		double dFMax;
		double dFMin;
		double dMeanF;
		double dDeltaF;
		double inv_lenX;
		double inv_intfWidth;
		double maxAmplitudeGrowth;


		string strOutFilePath;
		string strOutFileName;

		void intfFit();
		void drplFit();
		void bblFit();

	public:		
		bool DEBUG;

		vector<double> dAmplIntf;
		vector<double> devianceIntf;

		postProcessing(double* dFieldIn);
		void fit();
		void gnuPlot();
};