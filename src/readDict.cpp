// readDict.cpp
#include "readDict.h"

// readDict::readDict(){}


readDict::readDict(const string& strName){
	strDictName = strName;
	bRDFail = false;
	cout << "readDict::readDict	opening dictionary: " << strDictName << endl;
	iStrDict.open (strDictName.c_str(), fstream::in);
	if (! iStrDict.is_open() ){
		cout << "readDict::readDict	reading dictionary failed: file not found. aborting." << endl;
		bRDFail = true;
		return;
	}
	iStrDict.close();
}


string readDict::findKeyWordValue(const string& strKeyWord){
	iStrDict.open (strDictName.c_str(), fstream::in);
	string strBuffer;
	size_t pos = strBuffer.npos;		// strBuffer.npos returns the maximum size of size_t. It is returned by find if no match is found.
	size_t pos2;
	while ( pos == strBuffer.npos && getline(iStrDict, strBuffer) ){	// for as long as no position is found
		pos = strBuffer.find(strKeyWord);
	}
	// the correct line is in the buffer
	pos = strBuffer.find_first_not_of( " \t", pos+strKeyWord.length() );			// position of first value character
	pos2 = strBuffer.find_first_of(" \t\n", pos);									// position of first character after value
	strKeyWordValue.append(strBuffer,pos,pos2-pos);
	iStrDict.close();
	return strKeyWordValue;
}

void readDict::stringToType( const string& strInput, int& typeValue){
	// cout << "readDict::stringToType  changing "<< strInput << " to int." << endl;
	typeValue = stoi(strInput);	
	if(	typeValue/stod(strInput) != 1.0	){
		cout << "readDict::stringToType\tERROR: read value is not an integer. aborting." << endl;
		bRDFail = true;
	}
}

void readDict::stringToType( const string& strInput, string& typeValue){
	// cout << "readDict::stringToType  changing "<< strInput << " to double." << endl;
	typeValue = strInput;	
}

void readDict::stringToType( const string& strInput, double& typeValue){
	// cout << "readDict::stringToType  changing "<< strInput << " to double." << endl;
	typeValue = stod(strInput);	
}

void readDict::stringToType( const string& strInput, bool& typeValue){
	// cout << "readDict::stringToType  changing to bool." << endl;
	typeValue = toBool(strInput);
}

bool readDict::toBool(const string& strTB) {
	bool bRtn;
	if(strTB == "true" || strTB == "1"){
		// cout << "readDict::toBool\tboolian is true." << endl;
		bRtn = true;
	}else if(strTB == "false" || strTB == "0" ){
		bRtn = false;
	}else{
		cout << "readDict::toBool\tERROR: read value is not boolian. aborting." << endl;
		bRDFail = true;
		bRtn = false;
	}
	return bRtn;
}

void readDict::setGPParameter(const string& strParameterKeyWord, const string& strReplace){
	iStrDict.open (strDictName.c_str(), fstream::in);
	ofstream oStrDictIntr;
	oStrDictIntr.open(".gnuPlotScriptInterim",  ofstream::trunc );
	bool bFound = false;
	
	string strBuffer;
	size_t pos = strBuffer.npos;		// strBuffer.npos returns the maximum size of size_t. It is returned by find if no match is found.
	size_t len = strParameterKeyWord.length();
	while ( pos == strBuffer.npos && getline(iStrDict, strBuffer).good() ){	// for as long as no position is found
		if( pos != strBuffer.find(strParameterKeyWord) && bFound == false ){
			pos = strBuffer.find(strParameterKeyWord);
			strBuffer.erase(pos+len+1,100);

			oStrDictIntr << strBuffer;
			strBuffer = strReplace;
			pos = strBuffer.npos;
			bFound = true;
		}
		oStrDictIntr << strBuffer << endl;
	}
	oStrDictIntr << strBuffer << endl;
	oStrDictIntr.close();
// ---------------------copying file lines to file with same name---------------------
	iStrDict.close();
	ifstream iStrDictIntr;
	iStrDictIntr.open(".gnuPlotScriptInterim");
	
	ofstream oStrDictOut;
	oStrDictOut.open(strDictName.c_str(), ofstream::out | ofstream::trunc );
	while( getline(iStrDictIntr, strBuffer).good() ){
		oStrDictOut << strBuffer << endl;
	}
	oStrDictOut << strBuffer << endl;

	oStrDictOut.close();
	iStrDictIntr.close();
}

void readDict::findAndReplace(const string& strFind, const string& strReplace){
	iStrDict.open (strDictName.c_str(), fstream::in);
	ofstream oStrDictIntr;
	oStrDictIntr.open(".gnuPlotScriptInterim",  ofstream::trunc );
	
	string strBuffer;
	size_t pos = strBuffer.npos;		// strBuffer.npos returns the maximum size of size_t. It is returned by find if no match is found.
	size_t len = strFind.length();
	while ( pos == strBuffer.npos && getline(iStrDict, strBuffer).good() ){	// for as long as no position is found
		if( pos != strBuffer.find(strFind) ){
			pos = strBuffer.find(strFind);
			strBuffer = strBuffer;
			strBuffer.replace(pos,len,strReplace);
			pos = strBuffer.npos;
		}
		oStrDictIntr << strBuffer << endl;
	}
	oStrDictIntr << strBuffer << endl;
	oStrDictIntr.close();
// ---------------------copying file lines to file with same name---------------------
	iStrDict.close();
	ifstream iStrDictIntr;
	iStrDictIntr.open(".gnuPlotScriptInterim");
	
	ofstream oStrDictOut;
	oStrDictOut.open(strDictName.c_str(), ofstream::out | ofstream::trunc );
	while( getline(iStrDictIntr, strBuffer).good() ){
		oStrDictOut << strBuffer << endl;
	}
	oStrDictOut << strBuffer << endl;

	oStrDictOut.close();
	iStrDictIntr.close();
}

