// postProcessing.cpp
#include "postProcessing.h"

postProcessing::postProcessing(double* dFieldIn){
	
// -----------------------------reading dictinoary---------------------------
	readDict rd("parametersInv.txt");

	strOutFileName = rd.getKeyWordValue<string>("fileName");
	strOutFilePath = rd.getKeyWordValue<string>("fileOutputPath");
	DEBUG =  rd.getKeyWordValue<bool>("debug");
	iLenX = rd.getKeyWordValue<int>("lengthX");
	iIntfWidth = rd.getKeyWordValue<int>("intfWidth");
	dRelPertAmpl = rd.getKeyWordValue<double>("perturbationAmplitude");
	iLambda = rd.getKeyWordValue<int>("perturbationWaveLengthLambda");
	dAtwoodNo = rd.getKeyWordValue<double>("AtwoodNumber");
	dS = rd.getKeyWordValue<double>("surfaceTensionRelatedS");
	dKPrime = rd.getKeyWordValue<double>("kStar");
	iThreadNo = rd.getKeyWordValue<int>("numberOfThreads");
	bPrintIntfProp =  rd.getKeyWordValue<bool>("InterfaceFit");
	bPrintDropletProp =  rd.getKeyWordValue<bool>("DropletFit");
	bPrintBubbleProp =  rd.getKeyWordValue<bool>("BubbleFit");
	bGnuPlot =  rd.getKeyWordValue<bool>("gnuplotOutput");
	noFittingIterations	 = rd.getKeyWordValue<int>("noFittingIterations");
	maxAmplitudeGrowth = rd.getKeyWordValue<double>("maxAmplitudeGrowth");


// -----------------------------setting other variables---------------------------
	iLenY = 2*iLenX;
	iLenAr = iLenX*iLenY;

	dField = dFieldIn;

	bParallel = false;
	if( iThreadNo>1 )	bParallel = true;

	dAmplIntf.push_back(dRelPertAmpl*double(iLenX ));		//initial perturbation amplitude
	midY = 0.5*double(iLenY);
	dFMin = 1.0;
	dFMax = dFMin * ( 1.0 + dAtwoodNo ) / ( 1.0 - dAtwoodNo ); 
	dMeanF = 0.5*(dFMax + dFMin);
	dDeltaF = 0.5*(dFMax - dFMin);
	inv_lenX = 1.0/double(iLenX);
	inv_intfWidth =  1.0/double(iIntfWidth);
}


void postProcessing::fit(){


	if( bPrintIntfProp ){
		if(DEBUG) 	cout << "postProcessing::fit\tfitting whole interface." << endl;
		intfFit();
	}
	if( bPrintDropletProp ){
		if(DEBUG) 	cout << "postProcessing::fit\tfitting droplet." << endl;
		drplFit();
	}
	if( bPrintBubbleProp ){
		if(DEBUG)	cout << "postProcessing::fit\tfitting bubble." << endl;
		bblFit();
	}	
}


void postProcessing::drplFit(){
}

void postProcessing::bblFit(){
}

void postProcessing::intfFit(){
	double amplLoc = dAmplIntf.back();
	double deltaAmpl = maxAmplitudeGrowth * dAmplIntf.back() /double(noFittingIterations);
	double* dChiSq = new double[noFittingIterations];
	double dChiSqMin;
	double dAmplIntfMin = dAmplIntf.back();
	dChiSqMin = MEGA;
	amplLoc = amplLoc*0.9;

	for (int iIterCounter = 0; iIterCounter < noFittingIterations; iIterCounter++ ){
		dChiSq[iIterCounter] = 0.0;

		#pragma omp parallel num_threads( iThreadNo ) shared( dChiSq, iIterCounter, amplLoc ) default(none) if( bParallel )
		{	
			int b;					// index as used in data array
			double dXsq = 0.0;
			#pragma omp for
			for( int iX = 0; iX < iLenX; iX++){
				for( int iY = 0; iY < iLenY; iY++){		// adjust counting range
					b = iY*iLenX + iX;
					dXsq += pow( dField[b] - ( dMeanF + dDeltaF * tanh( 2.0 *inv_intfWidth * ( iY - midY - amplLoc * cos( iX * 2.0 * M_PI * inv_lenX ) ) ) ) ,2);
				}
			}
			#pragma omp atomic
			dChiSq[iIterCounter] += dXsq;
		}
		amplLoc += deltaAmpl;
		if( dChiSq[iIterCounter] < dChiSqMin ){
			dChiSqMin = dChiSq[iIterCounter];
			dAmplIntfMin = amplLoc;
			if(iIterCounter == noFittingIterations-1) cout << "postProcessing::intfFit\thighest fitting amplitude reached. Adjust amplitude increment." << endl;
			if(DEBUG) cout << "postProcessing::intfFit\tnew best amplitude found: " << dAmplIntfMin << "\tx^2 = "<< dChiSqMin << endl;
		}
	}

	dAmplIntf.push_back( dAmplIntfMin );
	devianceIntf.push_back(dChiSqMin);
}

void postProcessing::gnuPlot(){
	string strOutFileNamePath = strOutFilePath + strOutFileName + "_GnuPlotOut.csv";
	
// -----------------------writing data-------------------------
	double alpha;
	int iX, iY;
	FILE *gnuPlotFile = fopen(strOutFileNamePath.c_str(),"w");
	for ( int b=0; b<iLenAr; b++){
		iX = b%iLenX;
		iY = b/iLenX;
		alpha = dField[b];
		fprintf(gnuPlotFile,"%d \t %d \t %.10f \n",iY ,iX , alpha);
	} 
	fclose(gnuPlotFile);

// -----------------------opening data file in gnuPlot-------------------------
	string gpScrNm =  "gnuPlotScript";
	readDict gpScript( gpScrNm.c_str() );
	gpScript.setGPParameter("alpha", to_string( dAmplIntf.back() ) );
	gpScript.setGPParameter("beta", to_string( 2.0 * M_PI * inv_lenX ) );
	gpScript.setGPParameter("gamma", to_string( midY ) );
	gpScript.setGPParameter("delta","\"interfaceTrackingTest_GnuPlotOut.csv\"");		// double quotation marks

	FILE *gp;
	gp = popen("gnuplot -persist","w");
	fprintf(gp, "load \"%s\"\n", gpScrNm.c_str());
	fclose(gp);
}
